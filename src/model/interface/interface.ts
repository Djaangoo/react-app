export interface IUserData {
    name: string;
    img: string;
    id: number;
}
export interface IUserDataFull extends IUserData {
    friends: IUserData[];
}

export interface IComment {
    author: IUserData;
    text: string;
}

export interface IGroup {
    img: string;
    value: string;
}
export interface ISingleKudos {
    date: string;
    author: IUserData;
    text: string;
    kudos: IKudos;
    recipient: IUserData;
    likes: ILikes;
    comments: IComment[];
    group: IGroup;
    id: number;
}
export interface ILikes {
    clicked: boolean;
    counter: number;
}
export interface ISingleKudosReqest {
    date: string;
    author: IUserData;
    text: string;
    kudos: IKudos;
    recipient: IUserData;
    group: IGroup;
}
export interface IKudos {
    img: string;
    text: string;
    id: number;
}

export interface ISuggestion {
    name: string;
    link: string;
    avatar: string;
}
