import React, { FunctionComponent } from "react";

import grid from "../../assets/style/grid.scss";
import layout from "../../assets/style/layout.scss";
import style from "./AddKudos.module.scss";
import { IUserDataFull } from "../../model/interface/interface";

import { motion } from "framer-motion";

type AddKudosProps = {
    triggerOverlay: any;
    userData: IUserDataFull;
};

/**
 * Component using to trigger Overlay component
 * @param triggerOverlay - required, function which hide/show overlay component
 * @param userData - required, data of logged user
 */
export const AddKudos: FunctionComponent<AddKudosProps> = ({
    triggerOverlay,
    userData,
}) => (
    <motion.section
        initial={{ opacity: 0, x: 20 }}
        animate={{ opacity: 1, x: 0 }}
        transition={{
            delay: 0.1,
            duration: 0.3,
        }}
        className={style.addKudos}
        onClick={() => {
            triggerOverlay(true);
        }}
    >
        <div className={grid.row + " " + style.holder}>
            <span className={grid.col}>
                <div className={layout.thumb}>
                    <img src={userData.img} alt={userData.name} />
                </div>
            </span>
            <p className={grid.col__xs_10 + " " + style.text}>
                Kliknij, aby dodać post
            </p>
            <div className={grid.col + " " + style.agreement}>
                <span className={layout.icon}>
                    <img
                        src="/src/assets/img/icons/agreement.svg"
                        alt="Agreemnet"
                    />
                </span>
            </div>
        </div>
    </motion.section>
);
