import React, { FunctionComponent } from "react";

import style from "./NoMatch.module.scss";
/**
 * Component usig to shows wrong-urls error
 */
export const NoMatch: FunctionComponent = () => {
    const mess: string = "Ups... Coś poszło nie tak!";
    return <main className={style.wrapper}>{mess}</main>;
};
