import React, { FunctionComponent, useEffect } from "react";
import { EditorState, ContentState, SelectionState, Modifier } from "draft-js";

import grid from "../../assets/style/grid.scss";
import layout from "../../assets/style/layout.scss";
import style from "./Comments.module.scss";
import { IUserDataFull, IComment } from "../../model/interface/interface";

import { convertToHTML } from "draft-convert";

import { EditorComponent } from "../c-EditorComponent/EditorComponent";

type CommentsProps = {
    userData: IUserDataFull;
    commentsData: IComment[];
    updateKudos: any;
};

/**
 * Component which shows passed comments and show form to add new
 * @param updateKudos - required, function which emmit adding new comment
 * @param commentsData - required, list of objects using to show added comments
 * @param userData - required, data of logged user
 */
export const Comments: FunctionComponent<CommentsProps> = ({
    userData,
    commentsData,
    updateKudos,
}) => {
    let [editorState, setEditorState] = React.useState(
        EditorState.createEmpty()
    );
    let [hideEditor, triggerHideEditor] = React.useState(false);
    const changeEditorState = (state) => {
        setEditorState(state);
    };
    let editorConfig = {
        placeholder: "Napisz komentarz...",
    };
    const convertEditorState = (state) => {
        return convertToHTML({
            entityToHTML: (entity, originalText) => {
                if (entity.type === "mention" || entity.type === "#mention") {
                    return <a href={"#"}>{originalText}</a>;
                }
                return originalText;
            },
        })(state.getCurrentContent());
    };

    const _keyDown = (event) => {
        if (event.keyCode == 13 && !event.shiftKey) {
            updateKudos({
                comments: [
                    ...commentsData,
                    {
                        author: {
                            img: userData.img,
                            name: userData.name,
                            id: userData.id,
                        },
                        text: convertEditorState(editorState),
                    },
                ],
            });

            triggerHideEditor(false);
            const newEditorState = EditorState.push(
                editorState,
                ContentState.createFromText("")
            );

            setEditorState(newEditorState);
        }
    };

    return (
        <div className={style.comments}>
            {!hideEditor && (
                <div className={grid.row}>
                    <span className={grid.col}>
                        <div className={layout.thumb + " " + layout.small}>
                            <img src={userData.img} alt={userData.name} />
                        </div>
                    </span>
                    <div
                        className={style.message + " " + grid.col_11}
                        onKeyDown={_keyDown}
                    >
                        <EditorComponent
                            editorConfig={editorConfig}
                            friends={userData.friends}
                            editorState={editorState}
                            changeEditorState={changeEditorState}
                            modifer={"comment"}
                        />
                    </div>
                </div>
            )}
            <div className={grid.row + " " + style.added}>
                {commentsData.map((comment: IComment, index: number) => {
                    return (
                        <div
                            key={index}
                            className={grid.col_12 + " " + style.added__wrapper}
                        >
                            <span className={grid.col}>
                                <div
                                    className={
                                        layout.thumb + " " + layout.small
                                    }
                                >
                                    <img
                                        src={comment.author.img}
                                        alt={comment.author.name}
                                    />
                                </div>
                            </span>
                            <div
                                className={
                                    grid.col_11 + " " + style.added__text
                                }
                            >
                                <p className={style.added__name}>
                                    {comment.author.name}:
                                </p>
                                <div
                                    className={style.added__content}
                                    dangerouslySetInnerHTML={{
                                        __html: comment.text,
                                    }}
                                ></div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};
