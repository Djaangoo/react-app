import React, { FunctionComponent } from "react";

import style from "./Overlay.module.scss";
import { IUserDataFull } from "../../model/interface/interface";

type OverlayProps = {
    triggerOverlay: any;
    userData: IUserDataFull;
    changeListOfKudos: any;
    isActive?: boolean;
};
/**
 * HOC component, show passed component in fixed wrapper
 * @param triggerOverlay - required, function which show/hide overlay, passed to wrapped component
 * @param isActive - required, hide/show overlay status, passed to wrapped component
 * @param changeListOfKudos - required, function using to update list of Kudos Posts, passed to wrapped component
 * @param userData - required, data of logged user, passed to wrapped component
 */
export const Overlay = (WrappedComponent: FunctionComponent<OverlayProps>) => ({
    triggerOverlay,
    userData,
    changeListOfKudos,
}) => {
    return (
        <div className={style.overlay}>
            <div className={style.wrapper}>
                <WrappedComponent
                    changeListOfKudos={changeListOfKudos}
                    triggerOverlay={triggerOverlay}
                    userData={userData}
                />
            </div>
        </div>
    );
};
