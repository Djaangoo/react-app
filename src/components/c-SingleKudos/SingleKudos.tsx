import React, { FunctionComponent, useState } from "react";
import moment from "../../../node_modules/moment/moment";
import "moment/locale/pl";

import grid from "../../assets/style/grid.scss";
import layout from "../../assets/style/layout.scss";
import style from "./SingleKudos.module.scss";

import { Comments } from "../c-Comments/Comments";
import { ISingleKudos, IUserDataFull } from "../../model/interface/interface";
import { KudosType } from "../c-KudosType/KudosType";
import { fetchHelper } from "../../utils/helpers";
import api from "../../utils/api";

type SingleKudosProps = {
    kudosData: ISingleKudos;
    userData: IUserDataFull;
};
/**
 * Component shows kudos Post
 * @param userData - required, data of logged user
 * @param kudosData - required, object with Post content
 */
export const SingleKudos: FunctionComponent<SingleKudosProps> = ({
    kudosData,
    userData,
}) => {
    const [actionList, triggerActionList] = useState(false);
    const [kudosState, changeKudosState] = useState<ISingleKudos>(kudosData);

    const triggerLikes = () => {
        updateKudos({
            likes: {
                clicked: !kudosState.likes.clicked,
                counter: kudosState.likes.clicked
                    ? kudosState.likes.counter - 1
                    : kudosState.likes.counter + 1,
            },
        });
    };
    const updateKudos = (fieldsToUpdate) => {
        fetchHelper(api.updateKudos(kudosState.id), "POST", {
            ...kudosState,
            ...fieldsToUpdate,
        })
            .then((req) => req.json())
            .then((data: ISingleKudos) => {
                changeKudosState(data);
            });
    };
    moment.locale("pl");
    const formatedDate = moment(kudosState.date).fromNow();

    return (
        <article>
            <div className={style.singleKudos + " " + grid.col_12}>
                <div className={style.mainWrapper}>
                    <div className={grid.row_static}>
                        <div className={grid.col_11 + " " + style.author}>
                            <div className={layout.thumb}>
                                <img
                                    src={kudosState.author.img}
                                    alt={kudosState.author.name}
                                />
                            </div>
                            <div className={style.info}>
                                <span className={style.name}>
                                    {kudosState.author.name}
                                </span>
                                <span className={style.date}>
                                    {formatedDate}
                                </span>
                            </div>
                        </div>
                        <div className={grid.col + " " + style.agreement}>
                            <span className={layout.icon + " " + layout.bg}>
                                <img
                                    src="/src/assets/img/icons/agreement.svg"
                                    alt="Agreemnet"
                                />
                            </span>
                        </div>
                    </div>
                    <div
                        className={style.message + " " + grid.col_12}
                        dangerouslySetInnerHTML={{
                            __html: kudosState.text,
                        }}
                    ></div>

                    <KudosType
                        img={kudosState.kudos.img}
                        text={kudosState.kudos.text}
                        name={kudosState.recipient.name}
                        modifer={"large"}
                    />

                    <div className={style.footer + " " + grid.col_12}>
                        <div className={style.tag}>
                            <span className={layout.icon + " " + layout.bg}>
                                <img
                                    src={kudosState.group.img}
                                    alt={kudosState.group.value}
                                />
                            </span>
                            <p>{kudosState.group.value}</p>
                        </div>
                        <div className={style.social}>
                            <ul className={style.social__list}>
                                <li
                                    className={style.social__item}
                                    onClick={triggerLikes}
                                >
                                    <span className={layout.icon}>
                                        {kudosState.likes.clicked ? (
                                            <img
                                                src="/src/assets/img/icons/heart-red.svg"
                                                alt="Likes"
                                            />
                                        ) : (
                                            <img
                                                src="/src/assets/img/icons/heart.svg"
                                                alt="Likes"
                                            />
                                        )}
                                    </span>
                                    {kudosState.likes.counter}
                                </li>
                                <li className={style.social__item}>
                                    <span className={layout.icon}>
                                        <img
                                            src="/src/assets/img/icons/comment.svg"
                                            alt="Comments"
                                        />
                                    </span>
                                    {kudosState.comments.length}
                                </li>
                            </ul>
                            <div
                                className={
                                    style.social__item + " " + style.menu
                                }
                            >
                                <span
                                    className={layout.icon}
                                    onClick={() => {
                                        triggerActionList(!actionList);
                                    }}
                                >
                                    <img
                                        src="/src/assets/img/icons/menu.svg"
                                        alt="Menu"
                                    />
                                </span>
                                <ul
                                    className={
                                        style.actionList +
                                        " " +
                                        (actionList ? style.active : null)
                                    }
                                    onClick={() => {
                                        triggerActionList(!actionList);
                                    }}
                                >
                                    <li className={style.actionList__item}>
                                        <img
                                            src="/src/assets/img/icons/edit.svg"
                                            alt=""
                                        />
                                        <span>Edytuj</span>
                                    </li>
                                    <li className={style.actionList__item}>
                                        <img
                                            src="/src/assets/img/icons/share.svg"
                                            alt=""
                                        />
                                        <span>Udostępnij</span>
                                    </li>
                                    <li className={style.actionList__item}>
                                        <img
                                            src="/src/assets/img/icons/notification.svg"
                                            alt=""
                                        />
                                        <span>Śledź post</span>
                                    </li>
                                    <li className={style.actionList__item}>
                                        <img
                                            src="/src/assets/img/icons/warning.svg"
                                            alt=""
                                        />
                                        <span>Zgłoś</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Comments
                userData={userData}
                commentsData={kudosState.comments}
                updateKudos={updateKudos}
            />
        </article>
    );
};
