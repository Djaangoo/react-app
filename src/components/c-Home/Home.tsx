import React, { FunctionComponent, useState, useEffect } from "react";

import layout from "../../assets/style/layout.scss";
import style from "./Home.module.scss";

import { AddKudos } from "../c-AddKudos/AddKudos";
import { KudosList } from "../c-KudosList/KudosList";
import { Overlay } from "../c-Overlay/Overlay";
import { KudosForm } from "../c-KudosForm/KudosForm";

import { fetchHelper } from "../../utils/helpers";
import api from "../../utils/api";

import { motion, AnimatePresence } from "framer-motion";

/**
 * Component Using like wrapper for main content
 */
export const Home: FunctionComponent = () => {
    const OverlayComponent = Overlay(KudosForm);
    const [OverlayState, triggerOverlay] = useState({ formOverlay: false });
    const [UserState, triggerUser] = useState({
        id: 1,
        name: "Anna Korolczuk",
        img: "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
        friends: [
            {
                id: 2,
                name: "Barbara Klimowicz",
                img:
                    "/src/assets/img/images/guilherme-stecanella-_dH-oQF9w-Y-unsplash.jpg",
            },
            {
                id: 3,
                name: "Ada Kowalska",
                img:
                    "/src/assets/img/images/michael-dam-mEZ3PoFGs_k-unsplash.jpg",
            },
            {
                id: 4,

                name: "Iza Stańko",
                img:
                    "/src/assets/img/images/roberto-delgado-webb-AxI9niqj_60-unsplash.jpg",
            },
        ],
    });
    const [ListOfKudos, changeListOfKudos] = useState([]);
    const _triggerOverlay = (value: boolean) => {
        triggerOverlay({ formOverlay: value });
    };
    useEffect(() => {
        fetchHelper(api.kudosList, "GET")
            .then((resp) => resp.json())
            .then((data) => {
                changeListOfKudos(data);
            });
    }, []);

    return (
        <main className={style.home}>
            <h1 className={layout.srOnly}>Kudos Application</h1>
            <section className={layout.container}>
                <AddKudos
                    triggerOverlay={_triggerOverlay}
                    userData={UserState}
                />
                <KudosList ListOfKudos={ListOfKudos} userData={UserState} />
            </section>

            <AnimatePresence>
                {OverlayState.formOverlay && (
                    <motion.aside
                        key="overlay"
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        exit={{ opacity: 0 }}
                        transition={{ duration: 0.3 }}
                    >
                        <OverlayComponent
                            changeListOfKudos={changeListOfKudos}
                            triggerOverlay={_triggerOverlay}
                            userData={UserState}
                        />
                    </motion.aside>
                )}
            </AnimatePresence>
        </main>
    );
};
