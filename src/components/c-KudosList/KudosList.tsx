import React, { FunctionComponent, useRef, useEffect } from "react";
import gsap from "gsap";

import style from "./KudosList.module.scss";

import { motion } from "framer-motion";

import { SingleKudos } from "../c-SingleKudos/SingleKudos";
import { ISingleKudos, IUserDataFull } from "../../model/interface/interface";

type KudosListProps = {
    ListOfKudos: ISingleKudos[];
    userData: IUserDataFull;
};

/**
 * Component shows Posts which are generate based on @param ListOfKudos
 * @param commentsData - required, list of objects using to show added kudos Posts
 * @param userData - required, data of logged user
 */
export const KudosList: FunctionComponent<KudosListProps> = ({
    ListOfKudos,
    userData,
}) => {
    return (
        <motion.section
            initial={{
                y: 50,
                opacity: 0,
            }}
            animate={{
                y: 0,
                opacity: 1,
            }}
            transition={{
                delay: 1.7,
                duration: 0.3,
            }}
            className={style.kudosList}
        >
            {ListOfKudos.map((data: ISingleKudos, index: number) => {
                return (
                    <SingleKudos
                        kudosData={data}
                        userData={userData}
                        key={data.id}
                    />
                );
            })}
        </motion.section>
    );
};
