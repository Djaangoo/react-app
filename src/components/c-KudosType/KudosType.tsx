import React, { FunctionComponent } from "react";

import style from "./KudosType.module.scss";

type KudosTypeProps = {
    name?: string;
    text: string;
    img: string;
    isActive?: boolean;
    modifer?: string;
};
/**
 * Component shows Kudos element with image, text and recipient's name
 * @param name - optional, name of recipient
 * @param text - required, kudos message
 * @param img - required, kudos image src path
 * @param modifer - optional, using to shows diffrent style-variants
 * @param isActive - optional, if component active - background will be changed
 */
export const KudosType: FunctionComponent<KudosTypeProps> = ({
    name,
    text,
    img,
    modifer,
    isActive,
}) => (
    <div
        className={
            style.kudos +
            " " +
            (style[modifer] || null) +
            " " +
            (isActive ? style.isActive : null)
        }
    >
        <div className={style.kudos__img}>
            <img src={img} alt="" />
        </div>
        <div className={style.kudos__texts}>
            <p className={style.kudos__mess}>{text}</p>
            <p className={style.kudos__recipient}>{name || "Imię Nazwisko"}</p>
        </div>
    </div>
);
