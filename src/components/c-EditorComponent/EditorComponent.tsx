import React, { Component } from "react";
import { EditorState, ContentState } from "draft-js";

import layout from "../../assets/style/layout.scss";
import style from "./EditorComponent.module.scss";

const MAX_LENGTH = 500;

import Editor from "draft-js-plugins-editor";
import createEmojiPlugin from "draft-js-emoji-plugin";
import createCounterPlugin from "draft-js-counter-plugin";
import createMentionPlugin, {
    defaultSuggestionsFilter,
} from "draft-js-mention-plugin";

import { IUserData, ISuggestion } from "../../model/interface/interface";

type EditorProps = {
    editorState: any;
    changeEditorState: any;
    friends: IUserData[];
    modifer?: string;
    editorConfig?: any;
};
type EditorState = {
    editorState: any;
    mentionSuggestions: ISuggestion[];
    hashesSuggestions: ISuggestion[];
    isActive: boolean;
};

/**
 * Text field based on draft.js which support mentions, hashes, emoji, word counter
 * @param editorState - required, state of EditorState from draft.js
 * @param changeEditorState - required, function which emmit changes in @param editorState
 * @param friends - required, list of logged user friend, necessary to bild mentions
 * @param modifer - optional, using to shows diffrent style-variants
 * @param editorConfig - optional, object of attributes passed to Editor
 */
export class EditorComponent extends Component<EditorProps, EditorState> {
    private mentionPlugin = createMentionPlugin({ mentionPrefix: `@` });
    private hashtagPlugin = createMentionPlugin({
        mentionTrigger: `#`,
        mentionPrefix: `#`,
    });
    private emojiPlugin = createEmojiPlugin();
    private counterPlugin = createCounterPlugin({
        theme: {
            counter: style.counter,
        },
    });
    private EmojiSuggestions = this.emojiPlugin.EmojiSuggestions;
    private EmojiSelect = this.emojiPlugin.EmojiSelect;
    private CustomCounter = this.counterPlugin.CustomCounter;
    private HashSuggestions = this.hashtagPlugin.MentionSuggestions;
    private MentionSuggestions = this.mentionPlugin.MentionSuggestions;
    private editor;
    private mentions: ISuggestion[] = [];
    private hashes: ISuggestion[] = [];
    private wrapperRef: any;
    constructor(props) {
        super(props);
        this.wrapperRef = React.createRef();
        this.mentions = this.props.friends.map((friend: IUserData) => {
            return {
                name: friend.name,
                avatar: friend.img,
                link: "#",
            };
        });
        this.hashes = [
            {
                name: "funny",
                avatar: "/src/assets/img/icons/funny.svg",
                link: "#",
            },
            {
                name: "amazing",
                avatar: "/src/assets/img/icons/amazing.svg",
                link: "#",
            },
            {
                name: "team",
                avatar: "/src/assets/img/icons/team.svg",
                link: "#",
            },
        ];
    }

    state = {
        isActive: false,
        editorState: this.props.editorState,
        mentionSuggestions: this.mentions,
        hashesSuggestions: this.hashes,
    };

    _onChange = (editorState) => {
        this.props.changeEditorState(editorState);
    };

    focusEditor = () => {
        this.editor.focus();
        this.setState({ isActive: true });
    };

    searchMentions = ({ value }) => {
        this.setState({
            mentionSuggestions: defaultSuggestionsFilter(value, this.mentions),
        });
    };
    searchHashes = ({ value }) => {
        this.setState({
            hashesSuggestions: defaultSuggestionsFilter(value, this.hashes),
        });
    };
    charCounter = (text) => {
        return MAX_LENGTH - text.length;
    };
    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            editorState: nextProps.editorState,
        };
    }
    componentDidMount() {
        document.addEventListener(
            "mousedown",
            this.handleClickOutside.bind(this)
        );
    }

    componentWillUnmount() {
        document.removeEventListener(
            "mousedown",
            this.handleClickOutside.bind(this)
        );
    }
    handleClickOutside(event) {
        if (
            this.wrapperRef &&
            this.wrapperRef.current &&
            !this.wrapperRef.current.contains(event.target)
        ) {
            this.setState({ isActive: false });
        }
    }
    render() {
        return (
            <span
                ref={this.wrapperRef}
                className={
                    style.message +
                    " " +
                    style.message__input +
                    " " +
                    (this.state.isActive ? style.isActive : "") +
                    " " +
                    style[this.props.modifer]
                }
                onClick={this.focusEditor}
            >
                <Editor
                    ref={(element) => {
                        this.editor = element;
                    }}
                    editorState={this.state.editorState}
                    onChange={this._onChange}
                    plugins={[
                        this.emojiPlugin,
                        this.mentionPlugin,
                        this.hashtagPlugin,
                        this.counterPlugin,
                    ]}
                    {...this.props.editorConfig}
                />
                <this.EmojiSuggestions />
                <this.MentionSuggestions
                    onSearchChange={this.searchMentions}
                    suggestions={this.state.mentionSuggestions}
                />
                <this.HashSuggestions
                    onSearchChange={this.searchHashes}
                    suggestions={this.state.hashesSuggestions}
                />

                <this.CustomCounter
                    countFunction={this.charCounter}
                    editorState={this.state.editorState}
                    limit={MAX_LENGTH}
                />

                <div className={style.actionList}>
                    <div
                        className={
                            layout.icon +
                            " " +
                            layout.bg +
                            " " +
                            layout.small +
                            " " +
                            style.actionList__item
                        }
                    >
                        <img
                            src="/src/assets/img/icons/gif.svg"
                            alt="Agreemnet"
                        />
                    </div>
                    <div className={style.actionList__item}>
                        <this.EmojiSelect />
                    </div>
                    <div
                        className={
                            layout.icon +
                            " " +
                            layout.bg +
                            " " +
                            layout.small +
                            " " +
                            style.actionList__item
                        }
                    >
                        <img
                            src="/src/assets/img/icons/attachment.svg"
                            alt="Agreemnet"
                        />
                    </div>
                </div>
            </span>
        );
    }
}
