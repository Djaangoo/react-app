import React, { FunctionComponent, useState, useEffect, useRef } from "react";
import ReactTags from "react-tag-autocomplete";
import Select from "react-select";

import { EditorState } from "draft-js";
import { convertToHTML } from "draft-convert";

import grid from "../../assets/style/grid.scss";
import layout from "../../assets/style/layout.scss";
import style from "./KudosForm.module.scss";
import reactTag from "./react-tags.scss";

import {
    IKudos,
    IUserDataFull,
    ISingleKudosReqest,
    IGroup,
} from "../../model/interface/interface";
import { fetchHelper, shakeAnimation } from "../../utils/helpers";
import api from "../../utils/api";
import { KudosType } from "../c-KudosType/KudosType";
import { EditorComponent } from "../c-EditorComponent/EditorComponent";

type KudosFormProps = {
    triggerOverlay?: any;
    userData: IUserDataFull;
    changeListOfKudos: any;
};

/**
 * Form component using to add new Kudos,
 * @param triggerOverlay - required, function which show/hide overlay
 * @param changeListOfKudos - required, function using to update list of Kudos Posts
 * @param userData - required, data of logged user
 */
export const KudosForm: FunctionComponent<KudosFormProps> = ({
    triggerOverlay,
    userData,
    changeListOfKudos,
}) => {
    const [kudosType, triggerKudosTypes] = useState([]);
    const [tags, changeTags] = useState([]);
    const [selectedKudos, changeSelectedKudos] = useState<IKudos>();
    const [suggestions, changeSugggestions] = useState([]);
    const [selectedGroup, changeSelectedGroup] = useState<IGroup>({
        value: "Białystok",
        img: "/src/assets/img/icons/city.svg",
    });
    const groupWrapper = useRef();
    const kudosLabel = useRef();
    const recipientLabel = useRef();
    let [editorState, setEditorState] = React.useState(
        EditorState.createEmpty()
    );

    const handleDelete = (i) => {
        const newTags = tags.slice(0);
        newTags.splice(i, 1);
        changeTags([...newTags]);
    };
    const chooseKudos = (elem: IKudos) => {
        changeSelectedKudos(elem);
    };
    const handleAddition = (tag) => {
        const newTags = [].concat(tag);
        changeTags([...newTags]);
    };
    const fetchData = async () => {
        fetchHelper(api.kudosTypes, "GET")
            .then((resp) => resp.json())
            .then((data) => {
                triggerKudosTypes([...data]);
            });
    };

    const changeEditorState = (state) => {
        setEditorState(state);
    };
    const convertEditorState = (state) => {
        return convertToHTML({
            entityToHTML: (entity, originalText) => {
                if (entity.type === "mention" || entity.type === "#mention") {
                    return <a href={"#"}>{originalText}</a>;
                }
                return originalText;
            },
        })(state.getCurrentContent());
    };

    const sendForm = () => {
        let fetchData: ISingleKudosReqest = {
            date: new Date().toISOString(),
            author: userData,
            text: convertEditorState(editorState),
            kudos: selectedKudos,
            recipient: tags[0],
            group: selectedGroup,
        };
        fetchHelper(api.addKudos, "POST", fetchData)
            .then((req) => req.json())
            .then((data) => {
                triggerOverlay(false);
                changeListOfKudos(data);
            });
    };
    const validateForm = (event) => {
        event.preventDefault();
        if (tags[0] && selectedKudos) {
            sendForm();
        } else {
            shakeAnimation(kudosLabel.current);
            shakeAnimation(recipientLabel.current);
        }
    };
    const options = [
        {
            value: "Białystok",
            label: "/src/assets/img/icons/city.svg",
            customAbbreviation: "Białystok",
        },
        {
            value: "Marketing",
            label: "/src/assets/img/icons/hotel.svg",
            customAbbreviation: "Marketing",
        },
        {
            value: "Przyjaciele",
            label: "/src/assets/img/icons/friends.svg",
            customAbbreviation: "Przyjaciele",
        },
    ];

    const formatOptionLabel = ({ value, label, customAbbreviation }) => (
        <div className={style.option}>
            <span
                className={
                    layout.icon +
                    " " +
                    layout.bg +
                    " " +
                    layout.small +
                    " " +
                    style.option__img
                }
            >
                <img src={label} alt="Marketing" />
            </span>
            <div className={style.option__text}>{customAbbreviation}</div>
        </div>
    );
    const handleChange = (selectedOption) => {
        changeSelectedGroup({
            value: selectedOption.value,
            img: selectedOption.label,
        });
    };
    useEffect(() => {
        changeSugggestions([...userData.friends]);
        fetchData();
    }, []);

    return (
        <div className={style.kudosForm}>
            <h2
                className={
                    style.kudosForm__header +
                    " " +
                    style.header +
                    " " +
                    grid.row_static
                }
            >
                <span className={layout.icon}>
                    <img
                        src="/src/assets/img/icons/agreement-purple.svg"
                        alt="Agreemnet"
                    />
                </span>
                Utwórz Kudos
            </h2>
            <form className={style.form} onSubmit={validateForm}>
                <fieldset className={grid.row}>
                    <label
                        htmlFor="message"
                        className={style.header + " " + style.label}
                    >
                        Treść posta nad kudosem
                    </label>

                    <EditorComponent
                        friends={userData.friends}
                        editorState={editorState}
                        changeEditorState={changeEditorState}
                    />
                </fieldset>
                <fieldset className={grid.row}>
                    <label
                        ref={recipientLabel}
                        htmlFor="message"
                        className={style.header + " " + style.label}
                    >
                        Wybierz osobę, której przyznajesz kudos
                    </label>
                    <div
                        className={
                            style.autosuggest +
                            " " +
                            (tags.length ? reactTag.disable : null)
                        }
                    >
                        <ReactTags
                            tags={tags}
                            placeholder={"np. Babara"}
                            suggestions={suggestions}
                            handleDelete={handleDelete.bind(this)}
                            handleAddition={handleAddition.bind(this)}
                            classNames={{
                                root: reactTag.reactTag,
                                selectedTag: reactTag.selectedTag,
                                suggestions: reactTag.suggestions,
                                search: grid.col + " " + reactTag.search,
                                searchInput: reactTag.searchInput,
                            }}
                        />
                    </div>
                </fieldset>
                <fieldset className={grid.row}>
                    <h3
                        className={style.header + " " + style.moreSpace}
                        ref={kudosLabel}
                    >
                        Wybierz kudos
                    </h3>
                    <ul>
                        {kudosType.map((kudos: IKudos, index: number) => {
                            return (
                                <li
                                    className={style.kudosList}
                                    onClick={() => {
                                        chooseKudos(kudos);
                                    }}
                                    key={kudos.id}
                                >
                                    <KudosType
                                        isActive={
                                            selectedKudos &&
                                            selectedKudos.id === kudos.id
                                        }
                                        img={kudos.img}
                                        text={kudos.text}
                                    />
                                </li>
                            );
                        })}
                    </ul>
                </fieldset>
                <fieldset>
                    <div className={grid.row}>
                        <div className={grid.col_5}>
                            <label className={style.header + " " + style.label}>
                                Wybierz grupę
                            </label>
                            <div
                                className={style.selectHolder}
                                ref={groupWrapper}
                            >
                                <Select
                                    styles={{
                                        indicatorSeparator: () => {
                                            {
                                                display: "none";
                                            }
                                        },
                                    }}
                                    onChange={handleChange}
                                    defaultValue={options[0]}
                                    formatOptionLabel={formatOptionLabel}
                                    options={options}
                                />
                            </div>
                        </div>
                        <div className={grid.col + " " + style.btn}>
                            <button onClick={validateForm}>Opublikuj</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <div
                className={style.closeBtn}
                onClick={() => {
                    triggerOverlay(false);
                }}
            >
                <span className={layout.icon + " " + layout.bg}>
                    <img src="/src/assets/img/icons/close.svg" alt="Close" />
                </span>
            </div>
        </div>
    );
};
