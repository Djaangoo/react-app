import React, { FunctionComponent } from "react";
import { hot } from "react-hot-loader";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";

import { Home } from "../c-Home/Home";
import { NoMatch } from "../c-NoMatch/NoMatch";

import "normalize.css";
import "draft-js-emoji-plugin/lib/plugin.css";
import "draft-js-mention-plugin/lib/plugin.css";
import "draft-js-hashtag-plugin/lib/plugin.css";

import "../../assets/style/base.scss";

/**
 * Root component and  Route component
 */
const App: FunctionComponent = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Home}></Route>
                <Route component={NoMatch}></Route>
            </Switch>
        </Router>
    );
};

export default App;
