export default {
    userList: "http://localhost:3001/userList",
    user(id: string): string {
        return `http://localhost:3001/userList/${id}`;
    },
    kudosTypes: "http://localhost:3001/kudosTypes",
    kudosList: "http://localhost:3001/kudosList",
    singleKudos(id: number): string {
        return `http://localhost:3001/kudosList/${id}`;
    },
    kudosPage(to: string | number, from?: string | number): string {
        return `http://localhost:3001/kudosPage/${to}/${from + "/" || ""}`;
    },
    addKudos: "http://localhost:3001/addKudos/",
    updateKudos(id: number): string {
        return `http://localhost:3001/kudosUpdate/${id}`;
    },
};
