import gsap from "gsap";

export async function fetchHelper(
    url: string,
    method: "PUT" | "DELETE" | "POST" | "GET",
    data?
) {
    const response = await fetch(url, {
        method: method,
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    });

    return response;
}

export const shakeAnimation = (elem) => {
    gsap.fromTo(
        elem,
        0.1,
        { x: -10 },
        {
            x: 10,
            repeat: 3,
            yoyo: true,
            onComplete: function () {
                gsap.to(elem, 0.1, {
                    x: 0,
                });
            },
        }
    );
};
