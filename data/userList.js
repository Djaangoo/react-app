module.exports = [
    {
        id: 1,
        name: "Anna Korolczuk",
        img: "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
        friends: [
            {
                id: 2,
                name: "Barbara Klimowicz",
                img:
                    "/src/assets/img/images/guilherme-stecanella-_dH-oQF9w-Y-unsplash.jpg",
            },
            {
                id: 3,
                name: "Ada Kowalska",
                img:
                    "/src/assets/img/images/michael-dam-mEZ3PoFGs_k-unsplash.jpg",
            },
            {
                id: 4,

                name: "Iza Stańko",
                img:
                    "/src/assets/img/images/roberto-delgado-webb-AxI9niqj_60-unsplash.jpg",
            },
        ],
    },
    {
        id: 2,
        name: "Barbara Klimowicz",
        img:
            "/src/assets/img/images/guilherme-stecanella-_dH-oQF9w-Y-unsplash.jpg",
        friends: [
            {
                id: 1,
                name: "Anna Korolczuk",
                img:
                    "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
            },
            {
                id: 1,
                name: "Ada Kowalska",
                img:
                    "/src/assets/img/images/michael-dam-mEZ3PoFGs_k-unsplash.jpg",
            },
            {
                id: 4,
                name: "Iza Stańko",
                img:
                    "/src/assets/img/images/roberto-delgado-webb-AxI9niqj_60-unsplash.jpg",
            },
        ],
    },
    {
        id: 3,
        name: "Ada Kowalska",
        img: "/src/assets/img/images/michael-dam-mEZ3PoFGs_k-unsplash.jpg",
        friends: [
            {
                id: 2,
                name: "Barbara Klimowicz",
                img:
                    "/src/assets/img/images/guilherme-stecanella-_dH-oQF9w-Y-unsplash.jpg",
            },
            {
                id: 1,
                name: "Anna Korolczuk",
                img:
                    "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
            },
            {
                id: 4,
                name: "Iza Stańko",
                img:
                    "/src/assets/img/images/roberto-delgado-webb-AxI9niqj_60-unsplash.jpg",
            },
        ],
    },
    {
        id: 4,
        name: "Iza Stańko",
        img:
            "/src/assets/img/images/roberto-delgado-webb-AxI9niqj_60-unsplash.jpg",
        friends: [
            {
                id: 2,
                name: "Barbara Klimowicz",
                img:
                    "/src/assets/img/images/guilherme-stecanella-_dH-oQF9w-Y-unsplash.jpg",
            },
            {
                id: 3,
                name: "Ada Kowalska",
                img:
                    "/src/assets/img/images/michael-dam-mEZ3PoFGs_k-unsplash.jpg",
            },
            {
                id: 1,
                name: "Anna Korolczuk",
                img:
                    "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
            },
        ],
    },
];
