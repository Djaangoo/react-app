module.exports = [
    {
        id: 1,
        img: "/src/assets/img/images/dziekuje.svg",
        text: "Dziękuje Ci",
    },
    {
        id: 2,
        img: "/src/assets/img/images/gratulacje.svg",
        text: "Gratulacje!",
    },
    {
        id: 3,
        img: "/src/assets/img/images/gracz-zespolowy.svg",
        text: "Gracz zespołowy",
    },
    {
        id: 4,
        img: "/src/assets/img/images/przechodzisz.svg",
        text: "Przechodzisz samego (samą) siebie",
    },
    {
        id: 5,
        img: "/src/assets/img/images/pozytywny-wplyw.svg",
        text: "Pozytywny wpływ",
    },
    {
        id: 6,
        img: "/src/assets/img/images/swietny-wspolpracownik.svg",
        text: "Świetny współpracownik",
    },
];
