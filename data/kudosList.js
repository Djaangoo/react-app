module.exports = [
    {
        author: {
            id: 1,
            name: "Anna Korolczuk",
            img:
                "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
        },
        text:
            'Wielkie dzięki <a href="#">@Barbara Klimowicz</a> za współpracę przy ostatnim projekcie. Dzięki twojej pomocy poradziłam sobie z badaniami i analizą! Mam nadzieję że jeszcze wiele takich projektów przed nami.',
        kudos: {
            img: "/src/assets/img/images/swietny-wspolpracownik.svg",
            text: "Świetny współpracownik",
        },
        recipient: {
            id: 2,
            name: "Barbara Klimowicz",
            img:
                "/src/assets/img/images/guilherme-stecanella-_dH-oQF9w-Y-unsplash.jpg",
        },
        date: "2020-04-10",
        likes: {
            clicked: true,
            counter: 4,
        },
        comments: [],
        group: {
            img: "/src/assets/img/icons/friends.svg",
            value: "Przyjaciele",
        },
        id: 1,
    },
    {
        author: {
            id: 2,
            name: "Barbara Klimowicz",
            img:
                "/src/assets/img/images/guilherme-stecanella-_dH-oQF9w-Y-unsplash.jpg",
        },
        text: "Wszystko z tobą jest łatwiejsze",
        kudos: {
            img: "/src/assets/img/images/swietny-wspolpracownik.svg",
            text: "Świetny współpracownik",
        },
        recipient: {
            id: 1,
            name: "Anna Korolczuk",
            img:
                "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
        },
        date: "2020-04-20",
        likes: {
            clicked: true,
            counter: 4,
        },
        comments: [],
        group: {
            img: "/src/assets/img/icons/hotel.svg",
            value: "Marketing",
        },
        id: 2,
    },

    {
        author: {
            id: 3,
            name: "Ada Kowalska",
            img: "/src/assets/img/images/michael-dam-mEZ3PoFGs_k-unsplash.jpg",
        },
        text:
            'Wielkie dzięki <a href="#">@Barbara Klimowicz</a> za współpracę przy ostatnim projekcie. Dzięki twojej pomocy poradziłam sobie z badaniami i analizą! Mam nadzieję że jeszcze wiele takich projektów przed nami. <a href="#">#funny</a> <a href="#">#team</a>',
        kudos: {
            img: "/src/assets/img/images/swietny-wspolpracownik.svg",
            text: "Świetny współpracownik",
        },
        recipient: {
            id: 2,
            name: "Barbara Klimowicz",
            img:
                "/src/assets/img/images/guilherme-stecanella-_dH-oQF9w-Y-unsplash.jpg",
        },
        likes: {
            clicked: false,
            counter: 0,
        },
        comments: [
            {
                text:
                    'Wielkie dzięki <a href="#">@Barbara Klimowicz</a> za współpracę przy ostatnim projekcie. Dzięki twojej pomocy poradziłam sobie z badaniami i analizą! Mam nadzieję że jeszcze wiele takich projektów przed nami. <a href="#">#funny</a> <a href="#">#team</a>',
                author: {
                    id: 1,
                    name: "Anna Korolczuk",
                    img:
                        "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
                },
            },
            {
                text:
                    'Wielkie dzięki <a href="#">@Barbara Klimowicz</a> za współpracę przy ostatnim projekcie. Dzięki twojej pomocy poradziłam sobie z badaniami i analizą! Mam nadzieję że jeszcze wiele takich projektów przed nami. <a href="#">#funny</a> <a href="#">#team</a>',
                author: {
                    id: 1,
                    name: "Anna Korolczuk",
                    img:
                        "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
                },
            },
        ],
        date: "2020-04-30",
        group: {
            img: "/src/assets/img/icons/city.svg",
            value: "Białystok",
        },
        id: 3,
    },
    {
        author: {
            id: 1,
            name: "Anna Korolczuk",
            img:
                "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
        },
        text:
            'Wielkie dzięki <a href="#">@Barbara Klimowicz</a> za współpracę przy ostatnim projekcie. Dzięki twojej pomocy poradziłam sobie z badaniami i analizą! Mam nadzieję że jeszcze wiele takich projektów przed nami.',
        kudos: {
            img: "/src/assets/img/images/swietny-wspolpracownik.svg",
            text: "Świetny współpracownik",
        },
        recipient: {
            id: 2,
            name: "Barbara Klimowicz",
            img:
                "/src/assets/img/images/guilherme-stecanella-_dH-oQF9w-Y-unsplash.jpg",
        },
        date: "2020-05-01",
        likes: {
            clicked: false,
            counter: 4,
        },
        comments: [
            {
                text:
                    'Wielkie dzięki <a href="#">@Barbara Klimowicz</a> za współpracę przy ostatnim projekcie. Dzięki twojej pomocy poradziłam sobie z badaniami i analizą! Mam nadzieję że jeszcze wiele takich projektów przed nami. <a href="#">#funny</a> <a href="#">#team</a>',
                author: {
                    id: 1,
                    name: "Anna Korolczuk",
                    img:
                        "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
                },
            },
            {
                text:
                    'Wielkie dzięki <a href="#">@Barbara Klimowicz</a> za współpracę przy ostatnim projekcie. Dzięki twojej pomocy poradziłam sobie z badaniami i analizą! Mam nadzieję że jeszcze wiele takich projektów przed nami. <a href="#">#funny</a> <a href="#">#team</a>',
                author: {
                    id: 1,
                    name: "Anna Korolczuk",
                    img:
                        "/src/assets/img/images/ayo-ogunseinde-6W4F62sN_yI-unsplash.jpg",
                },
            },
        ],
        group: {
            img: "/src/assets/img/icons/friends.svg",
            value: "Przyjaciele",
        },
        id: 4,
    },
];
