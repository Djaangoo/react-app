module.exports = {
    author: {
        id: 0,
        name: "",
        img: "",
    },
    text: "",
    kudos: {
        img: "",
        text: "",
    },
    recipient: {
        id: 0,
        name: "",
        img: "",
    },
    date: "",
    likes: {
        clicked: false,
        counter: 0,
    },
    comments: [],
    group: {
        img: "",
        value: "",
    },
    id: 0,
};
