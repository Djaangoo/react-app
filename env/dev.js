const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const port = 3001;

const userList = require("../data/userList");
const kudosTypes = require("../data/kudosTypes");
const kudosList = require("../data/kudosList");
const singleKudosModel = require("../data/model/singleKudos");

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/userList", (req, res) => {
    console.log("get /userList");
    res.json(userList);
});

app.get("/userList/:id", (req, res) => {
    const id = req.params.id;
    console.log(`get /userList/${id}`);
    const wantedUser = userList.find((user) => {
        return user.id == id;
    });
    if (wantedUser) {
        res.json(wantedUser);
    } else {
        res.status(400).send({ error: "User not found" });
    }
});

app.get("/kudosTypes", (req, res) => {
    console.log("get /kudosTypes");
    res.json(kudosTypes);
});

app.get("/kudosList", (req, res) => {
    console.log("get /kudosList");
    res.json(kudosList);
});

app.get("/kudosList/:id", (req, res) => {
    const id = req.params.id;
    console.log(`get /kudosList/${id}`);
    const wantedKudos = kudosList.find((kudos) => {
        return kudos.id == id;
    });
    if (wantedKudos) {
        res.json(wantedKudos);
    } else {
        res.status(400).send({ error: "Kudos not found" });
    }
});

app.post("/addKudos/", (req, res) => {
    let newKudos = req.body;

    const requiredFileds = [
        "text",
        "date",
        "kudos",
        "recipient",
        "group",
        "author",
    ];
    const isValidOperation = Object.keys(newKudos).every((field) => {
        return requiredFileds.includes(field);
    });

    if (!isValidOperation) {
        return res.status(400).send({ error: "Invalid kudos!" });
    }

    newKudos = { ...singleKudosModel, ...newKudos, id: kudosList.length + 1 };
    kudosList.push(newKudos);
    res.send(kudosList);
});

app.post("/kudosUpdate/:id", (req, res) => {
    const id = req.params.id;
    let updatedKudos = {};

    kudosList.forEach((kudos, index) => {
        if (kudos.id == id) {
            kudosList[index] = {
                ...req.body,
            };
            updatedKudos = kudosList[index];
            console.log("update", updatedKudos);
        }
    });
    res.send(updatedKudos);
});

app.get("/kudosPage/:to/(:from)?", (req, res) => {
    const from = req.params.from || 0,
        to = req.params.to;
    console.log(`get /kudosList/${to}/${from}`);
    const wantedKudos = kudosList.slice(from, to);
    if (wantedKudos) {
        res.json(wantedKudos);
    } else {
        res.status(400).send({ error: "Kudos not found" });
    }
});

app.listen(port, () => console.log(`App listening on port ${port}!`));
